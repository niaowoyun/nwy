jQuery(document).ready(function($){
	
	$('img').mouseover(function(){
		var orginalImg = $(this).attr('src');
		var hoverImg = $(this).parent('a').data('hover');
		if(hoverImg != ''){
			$(this).attr('src', hoverImg);
			$(this).parent('a').data('hover', orginalImg);
		}
	}).mouseout(function(){
		var orginalImg = $(this).attr('src');
		var hoverImg = $(this).parent('a').data('hover');
		if(hoverImg != ''){
			$(this).attr('src', hoverImg);
			$(this).parent('a').data('hover', orginalImg);
		}

	});
})