<!DOCTYPE html>
<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/common.jsp"%>
<!--头部开始-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>登录-鸟窝云</title>
	<meta name="Keywords"
		content="windows,linux,android,mac,os,ios,online,nw,鸟窝,鸟窝云,云,云端,在线操作系统,操作系统,服务器,云服务器,在线,鸟" />
	<meta name="Description"
		content="鸟窝云把本地的操作系统搬上了云端，让你不论身处何地，只要可以上网，就可以拥有一个在线的操作系统，不论是windows、linux、android还是ios，都可以随时随地瞬间切换，无需安装和漫长的等待，你可以用它来看电影、玩游戏，甚至做一个web服务器，只要你愿意！" />

	<!--字体加载开始-->
	<STYLE TYPE="text/css">
<!--
@font-face {
	font-family: myfont;
	font-style: normal;
	src: url(<%=request.getContextPath()%>/ css/ myfont.ttf );
}
-->
</STYLE>
	<STYLE TYPE="text/css">
<!--
@font-face {
	font-family: myfont2;
	font-style: normal;
	src: url(<%=request.getContextPath()%>/ css/ myfont2.ttf );
}
-->
</STYLE>
	<!--字体加载结束-->
</head>
<!--头部结束-->
<body class="login subpage  ltr preset2 responsive bg hfeed clearfix">
	<div class="body-innerwrapper" id="rox_main_body">
		<section id="sp-tob_bar-wrapper" class=" ">
		<div class="container">
			<div class="row-fluid" id="tob_bar">
				<div id="sp-topbar_1" class="span12">
					<!--顶部导航开始-->
					<ul class="nav ">
						<li class="item-158">
							<a href="Company.htm"><FONT FACE="myfont">联系我们</FONT> </a>
						</li>
						<li class="item-159">
							<a href="404.htm"><FONT FACE="myfont">网站地图</FONT> </a>
						</li>
						<li class="item-160">
							<a href="<%=request.getContextPath()%>/login/login.jsp"><FONT
								FACE="myfont">登录</FONT> </a>
						</li>
					</ul>
					<!--顶部导航结束-->
				</div>
			</div>
		</div>
		</section>
		<!--头部导航开始-->
		<header id="sp-header-wrapper" class=" ">
		<div class="row-fluid" id="header">
			<!--LOGO开始-->
			<div id="sp-logo" class="span4">
				<div class="logo-wrapper">
					<a href="<%=request.getContextPath()%>/index.jsp"><div
							style="width: 100%; height: 100px;" class="logo"></div> </a>
				</div>
			</div>
			<!--LOGO结束-->
			<!--导航开始-->
			<div id="sp-menu" class="span8">
				<div class="mobile-menu  btn hidden-desktop" id="sp-mobile-menu"></div>
				<div class="rox_menu_wraper">
					<div class="rox_menu_wraper2">
						<div id="sp-main-menu" class="visible-desktop">
							<ul class="sp-menu level-0">
								<li class="menu-item first">
									<a href="index.jsp" class="menu-item first"><span
										class="menu"><img class="menu-image"
												src="<%=request.getContextPath()%>/img/home-icon.png"
												alt=" "><span class="menu-title">&nbsp;</span> </span> </a>
								</li>
								<li class="menu-item parent ">
									<a href="xitong.htm" class="menu-item parent "> <span
										class="menu"> <span class="menu-title"><FONT
												FACE="myfont">在线系统</FONT> </span> </span> </a>
								</li>
								<li class="menu-item parent ">
									<a href="bangzhu.htm" class="menu-item parent "> <span
										class="menu"> <span class="menu-title"><FONT
												FACE="myfont">帮助文档</FONT> </span> </span> </a>
								</li>
								<li class="menu-item parent ">
									<a href="news.htm" class="menu-item parent "> <span
										class="menu"> <span class="menu-title"><FONT
												FACE="myfont">新闻动态</FONT> </span> </span> </a>
								</li>
								<li class="menu-item parent">
									<a href="baojia.htm" class="menu-item parent"><span
										class="menu"> <span class="menu-title"><FONT
												FACE="myfont">服务报价</FONT> </span> </span> </a>
								</li>
								<li class="menu-item parent ">
									<a href="login.htm" class="menu-item parent "><span
										class="menu"> <span class="menu-title"><FONT
												FACE="myfont">个人中心</FONT> </span> </span> </a>
								</li>
								<li class="menu-item parent ">
									<a
										href="http://tieba.baidu.com/f?kw=%E9%B8%9F%E7%AA%9D%E4%BA%91&ie=utf-8"
										target="_black"> <span class="menu"> <span
											class="menu-title"><FONT FACE="myfont">交流论坛</FONT> </span> </span> </a>
								</li>
								<li class="menu-item last">
									<a href="Company.htm" class="menu-item last"> <span
										class="menu-title"><FONT FACE="myfont">关于我们</FONT> </span></span> </a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!--导航结束-->
		</div>
		</header>
		<!--头部导航结束-->
		<!--现在位置开始-->
		<section id="sp-breadcrumb-wrapper" class=" ">
		<div class="container">
			<div class="row-fluid" id="breadcrumb">
				<div id="sp-breadcrumb" class="span12">
					<h3>
						<FONT FACE="myfont2">找回密码</FONT>
					</h3>
					<ul class="breadcrumb ">
						<li class="active">
							<span class="divider"> </span>
						</li>
						<li>
							<a href="<%=request.getContextPath()%>/index.jsp" class="pathway"><FONT FACE="myfont">首页</font>
							</a>
						</li>
						<li>
							<span class="divider">/</span><a href="<%=request.getContextPath()%>/login/login.jsp"><span><FONT
									FACE="myfont">登录</font>
							</a></span>
						</li>
						<li>
							<span class="divider">/</span><span><FONT FACE="myfont">找回密码</font>
							</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
		</section>
		<!--现在位置结束-->
		<!--登录开始-->
		<section id="sp-main-body-wrapper" class=" ">
		<div class="container">
			<div class="row-fluid" id="main-body">
				<div id="sp-message-area" class="span12">
					<div id="system-message-container">
						<div id="system-message">
						</div>
					</div>
					<section id="sp-content-area-wrapper" class=" ">
					<div class="row-fluid" id="content-area">
						<div id="sp-component-area" class="span12">
							<section id="sp-component-wrapper">
							<div id="sp-component">
								<div class="login ">
									<form action="#" method="post" class="form-horizontal">
										<fieldset>
											<div class="control-group">
												<div class="control-label">
													<label id="username-lbl" for="username" class="">
														<FONT FACE="myfont">注册邮箱</font>
													</label>
												</div>
												<div class="controls">
													<input type="text" name="username" id="username" value=""
														class="validate-username" size="25">
												</div>
											</div>
											<div class="control-group">
												<div class="controls">
													<button type="submit">
														<FONT FACE="myfont">发送</font>
													</button>
												</div>
											</div>
											<input type="hidden" name="return"
												value="aW5kZXgucGhwP29wdGlvbj1jb21fdXNlcnMmdmlldz1wcm9maWxl">
											<input type="hidden" name="44dcc46bbcab04c4430efe2718cd5eff"
												value="1">
											<div class="control-group">
												<div class="controls">
													<ul class="unstyled">
														<li>
															<FONT FACE="myfont">我们会向您的注册邮箱发送一封邮件，请您在收到邮件后及时更改密码</font>
														</li>
														<li>
															<FONT FACE="myfont">如果您的邮箱遇到意外，您可以致电：400-888-888</font>
														</li>
													</ul>
												</div>
											</div>
										</fieldset>
									</form>
								</div>
							</div>
							</section>
						</div>
					</div>
					</section>
				</div>
			</div>
		</div>
		</section>
		<!--底部开始-->
		<footer id="sp-footer-wrapper" class=" ">
		<div class="container">
			<div class="row-fluid" id="footer">
				<div id="sp-footer1" class="span7">
					<a class="copyright" href="#"> </a><span class="copyright">Copyright
						© 2015 nwyun.com. All Rights Reserved.</span>
				</div>
			</div>
		</div>
		</footer>
		<!--底部结束-->
</body>
<!--主体结束-->
</html>