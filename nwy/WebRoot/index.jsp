<!DOCTYPE html>
<%@ include file="/commons/common.jsp"%>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>鸟窝云-在线操作系统平台</title>
	<meta name="Keywords"
		content="windows,linux,android,mac,os,ios,online,nw,鸟窝,鸟窝云,云,云端,在线操作系统,操作系统,服务器,云服务器,在线,鸟" />
	<meta name="Description"
		content="鸟窝云把本地的操作系统搬上了云端，让你不论身处何地，只要可以上网，就可以拥有一个在线的操作系统，不论是windows、linux、android还是ios，都可以随时随地瞬间切换，无需安装和漫长的等待，你可以用它来看电影、玩游戏，甚至做一个web服务器，只要你愿意！" />
	<script src="./js/mootools-core.js" type="text/javascript"></script>
	<script src="./js/caption.js" type="text/javascript"></script>
	<script src="./js/jquery-noconflict.js" type="text/javascript"></script>
	<script src="./js/bootstrap.min.js" type="text/javascript"></script>
	<script src="./js/modernizr-2.6.2.min.js" type="text/javascript"></script>
	<script src="./js/acymailing_module.js" type="text/javascript"></script>
	<script src="./js/bootstrap-transition.js" type="text/javascript"></script>
	<script src="./js/bootstrap-carousel.js" type="text/javascript"></script>
	<script src="./js/jquery.themepunch.plugins.min.js"
		type="text/javascript"></script>
	<script src="./js/jquery.themepunch.revolution.min.js"
		type="text/javascript"></script>
	<script src="./js/helix.core.js" type="text/javascript"></script>
	<script src="./js/main.js" type="text/javascript"></script>
	<script src="./js/core.js" type="text/javascript"></script>
	<script src="./js/js.js" type="text/javascript"></script>
	<script src="./js/k2.js" type="text/javascript"></script>
	<script src="./js/ga.js" type="text/javascript"></script>
	<!--字体加载开始-->
	<STYLE TYPE="text/css">
<!--
@font-face {
	font-family: myfont;
	font-style: normal;
	src: url(css/myfont.ttf);
}
-->
</STYLE>
	<STYLE TYPE="text/css">
<!--
@font-face {
	font-family: myfont2;
	font-style: normal;
	src: url(css/myfont2.ttf);
}
-->
</STYLE>
	<!--字体加载结束-->

	<script type="text/javascript">
	window.addEvent('load', function() {
		new JCaption('img.caption');
	});
</script>
</head>
<!--头部结束-->
<!--主体开始-->
<body
	class="featured homepage  ltr preset2 responsive bg hfeed clearfix">
	<div class="body-innerwrapper" id="rox_main_body">
		<section id="sp-tob_bar-wrapper" class=" ">
		<div class="container">
			<div class="row-fluid" id="tob_bar">
				<div id="sp-topbar_1" class="span12">
					<!--顶部导航开始-->
					<ul class="nav ">
						<li class="item-158">
							<a href="commons/company.htm"><FONT FACE="myfont">联系我们</FONT>
							</a>
						</li>
						<li class="item-159">
							<a href="404.htm"><FONT FACE="myfont">网站地图</FONT> </a>
						</li>
						<li class="item-160">
							<a href="<%=request.getContextPath()%>/login/login.jsp"><FONT FACE="myfont">登录</FONT> </a>
						</li>
					</ul>
					<!--顶部导航结束-->
				</div>
			</div>
		</div>
		</section>
		<!--头部导航开始-->
		<header id="sp-header-wrapper" class=" ">
		<div class="row-fluid" id="header">
			<!--LOGO开始-->
			<div id="sp-logo" class="span4">
				<div class="logo-wrapper">
					<a href="index.jsp"><div style="width: 100%; height: 100px;"
							class="logo"></div> </a>
				</div>
			</div>
			<!--LOGO结束-->
			<!--导航开始-->
			<div id="sp-menu" class="span8">
				<div class="mobile-menu  btn hidden-desktop" id="sp-mobile-menu"></div>
				<div class="rox_menu_wraper">
					<div class="rox_menu_wraper2">
						<div id="sp-main-menu" class="visible-desktop">
							<ul class="sp-menu level-0">
								<li class="menu-item first">
									<a href="index.jsp" class="menu-item first"><span
										class="menu"><img class="menu-image"
												src="./img/home-icon.png" alt=" "><span
											class="menu-title">&nbsp;</span> </span> </a>
								</li>
								<li class="menu-item parent ">
									<a href="xitong.htm" class="menu-item parent "> <span
										class="menu"> <span class="menu-title"><FONT
												FACE="myfont">在线系统</FONT> </span> </span> </a>
								</li>
								<li class="menu-item parent ">
									<a href="bangzhu.htm" class="menu-item parent "> <span
										class="menu"> <span class="menu-title"><FONT
												FACE="myfont">帮助文档</FONT> </span> </span> </a>
								</li>
								<li class="menu-item parent ">
									<a href="news.htm" class="menu-item parent "> <span
										class="menu"> <span class="menu-title"><FONT
												FACE="myfont">新闻动态</FONT> </span> </span> </a>
								</li>
								<li class="menu-item parent">
									<a href="baojia.htm" class="menu-item parent"><span
										class="menu"> <span class="menu-title"><FONT
												FACE="myfont">服务报价</FONT> </span> </span> </a>
								</li>
								<li class="menu-item parent ">
									<a href="login.htm" class="menu-item parent "><span
										class="menu"> <span class="menu-title"><FONT
												FACE="myfont">个人中心</FONT> </span> </span> </a>
								</li>
								<li class="menu-item parent ">
									<a
										href="http://tieba.baidu.com/f?kw=%E9%B8%9F%E7%AA%9D%E4%BA%91&ie=utf-8"
										target="_black"> <span class="menu"> <span
											class="menu-title"><FONT FACE="myfont">交流论坛</FONT> </span> </span> </a>
								</li>
								<li class="menu-item last">
									<a href="Company.htm" class="menu-item last"> <span
										class="menu-title"><FONT FACE="myfont">关于我们</FONT> </span></span> </a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!--导航结束-->
		</div>
		</header>
		<!--头部导航结束-->
		<!--轮播图开始-->
		<section id="sp-feature-wrapper" class=" hidden-phone hidden-tablet">
		<div class="container">
			<div class="row-fluid" id="feature">
				<div id="sp-feature" class="span12">
					<!-- START REVOLUTION SLIDER ver. 2.2.4 -->
					<div id="rev_slider_1_1_wrapper"
						class="rev_slider_wrapper fullwidthbanner-container"
						style="margin-left: 0px auto; padding: 0px; max-height: 450px; direction: ltr; overflow: visible;">
						<div id="rev_slider_1_1"
							class="rev_slider fullwidthabanner revslider-initialised tp-simpleresponsive hovered"
							style="max-height: 450px; height: 450px;">
							<ul
								style="display: block; overflow: hidden; width: 100%; height: 100%; max-height: 450px;">
								<li data-transition="random" data-slotamount="7"
									data-masterspeed="300"
									style="width: 100%; height: 100%; overflow: hidden; visibility: visible; left: 0px; top: 0px; z-index: 18; opacity: 1;">
									<div class="slotholder">
										<img src="img/slider_bg.png" alt="" class="defaultimg"
											style="width: 1220px; height: 4880px; position: absolute; left: -25px; opacity: 0;">
									</div>
									<div class="tp-caption lft start" data-x="412" data-y="46"
										data-speed="300" data-start="500" data-easing="easeOutExpo"
										style="opacity: 1; left: 412px; top: -355px; transform: scale(1, 1) rotate(0deg); visibility: hidden;">
										<img src="img/imac.png" alt="Image 1"
											style="width: 363px; height: 295px;">
									</div>
									<div class="tp-caption randomrotate start" data-x="248"
										data-y="204" data-speed="300" data-start="800"
										data-easing="easeOutExpo"
										style="left: 952.418611126486px; top: 58.7536548846401px; opacity: 0; transform: scale(2.0735757897608, 2.0735757897608) rotate(4.6467887237668deg) translate(0px, 0px); visibility: visible;">
										<img src="img/mac_book.png" alt="Image 2"
											style="width: 324px; height: 173px;">
									</div>
									<div class="tp-caption lfb start" data-x="661" data-y="175"
										data-speed="300" data-start="1100" data-easing="easeOutExpo"
										style="opacity: 1; left: 661px; top: 510px; transform: scale(1, 1) rotate(0deg); visibility: hidden;">
										<img src="img/ipad.png" alt="Image 3"
											style="width: 144px; height: 181px;">
									</div>
									<div class="tp-caption lfr start" data-x="843" data-y="216"
										data-speed="300" data-start="1400" data-easing="easeOutExpo"
										style="opacity: 1; left: 1230px; top: 216px; transform: scale(1, 1) rotate(0deg); visibility: hidden;">
										<img src="img/ipad_mini.png" alt="Image 4"
											style="width: 95px; height: 143px;">
									</div>
									<div class="tp-caption lft start" data-x="786" data-y="253"
										data-speed="300" data-start="1700" data-easing="easeOutExpo"
										style="opacity: 1; left: 786px; top: -188px; transform: scale(1, 1) rotate(0deg); visibility: hidden;">
										<img src="img/i_phone.png" alt="Image 5"
											style="width: 69px; height: 128px;">
									</div>
								</li>
								<li data-transition="random" data-slotamount="7"
									data-masterspeed="300"
									style="width: 100%; height: 100%; overflow: hidden; visibility: visible; left: 0px; top: 0px; z-index: 20; opacity: 1;">
									<div class="slotholder">
										<img src="img/slider_bg.png" alt="" class="defaultimg"
											style="width: 1220px; height: 4880px; position: absolute; left: -25px; opacity: 1;">
									</div>
									<div class="tp-caption lfb start" data-x="641" data-y="37"
										data-speed="300" data-start="500" data-easing="easeOutExpo"
										style="opacity: 1; left: 641px; top: 37px; transform: scale(1, 1) rotate(0deg); visibility: visible;">
										<img src="img/imac.png" alt="Image 1"
											style="width: 363px; height: 295px;">
									</div>
									<div class="tp-caption lft start" data-x="520" data-y="204"
										data-speed="300" data-start="800" data-easing="easeOutExpo"
										style="opacity: 1; left: 520px; top: 204px; transform: scale(1, 1) rotate(0deg); visibility: visible;">
										<img src="img/mac_book.png" alt="Image 2"
											style="width: 324px; height: 173px;">
									</div>
									<div class="tp-caption lft start" data-x="981" data-y="199"
										data-speed="300" data-start="1100" data-easing="easeOutExpo"
										style="opacity: 1; left: 981px; top: 199px; transform: scale(1, 1) rotate(0deg); visibility: visible;">
										<img src="img/ipad.png" alt="Image 3"
											style="width: 144px; height: 181px;">
									</div>
									<div class="tp-caption rox_caption lft start" data-x="103"
										data-y="72" data-speed="300" data-start="1400"
										data-easing="easeOutExpo"
										style="font-size: 15px; padding: 7px 25px; margin: 0px; border: 1px; line-height: 20px; white-space: nowrap; opacity: 1; left: 103px; top: 72px; transform: scale(1, 1) rotate(0deg); visibility: visible;">
										<FONT FACE="myfont">一个账号瞬间让你的拥有所有操作系统、无需安装、在线使用</FONT>
									</div>
									<div class="tp-caption rox_caption lft start" data-x="103"
										data-y="116" data-speed="300" data-start="1700"
										data-easing="easeOutExpo"
										style="font-size: 15px; padding: 7px 25px; margin: 0px; border: 1px; line-height: 20px; white-space: nowrap; opacity: 1; left: 103px; top: 116px; transform: scale(1, 1) rotate(0deg); visibility: visible;">
										<FONT FACE="myfont">你可以用它干你想干的一切你觉得酷的事情</FONT>
									</div>
									<div class="tp-caption rox_caption lft start" data-x="103"
										data-y="161" data-speed="300" data-start="2000"
										data-easing="easeOutExpo"
										style="font-size: 15px; padding: 7px 25px; margin: 0px; border: 1px; line-height: 20px; white-space: nowrap; opacity: 1; left: 103px; top: 161px; transform: scale(1, 1) rotate(0deg); visibility: visible;">
										<FONT FACE="myfont">标准版是完全免费的</FONT>
									</div>
									<div class="tp-caption rox_button lft start" data-x="103"
										data-y="317" data-speed="300" data-start="2300"
										data-easing="easeOutExpo"
										style="font-size: 15px; padding: 0px; margin: 0px; border: 0px; line-height: 20px; white-space: nowrap; opacity: 1; left: 103px; top: 317px; transform: scale(1, 1) rotate(0deg); visibility: visible;">
										<div class="btnWrap">
											<a class="nemobtn" href="login.htm"><FONT FACE="myfont">立刻开始</FONT>
											</a>
										</div>
									</div>
								</li>
								<li data-transition="random" data-slotamount="7"
									data-masterspeed="300"
									style="width: 100%; height: 100%; overflow: hidden; visibility: visible; left: 0px; top: 0px; z-index: 18; opacity: 1;">
									<div class="slotholder">
										<img src="img/slider_bg.png" alt="" class="defaultimg"
											style="width: 1220px; height: 4880px; position: absolute; left: -25px; opacity: 0;">
									</div>
									<div class="tp-caption rox_caption lft start" data-x="103"
										data-y="72" data-speed="300" data-start="1400"
										data-easing="easeOutExpo"
										style="top: -80px; visibility: hidden; font-size: 18px; padding: 7px 25px; margin: 0px; border: 1px; line-height: 20px; white-space: nowrap; opacity: 1; left: 103px; transform: scale(1, 1) rotate(0deg);">
										Dedicated Clients Support For All Of Our Clients
									</div>
									<div class="tp-caption rox_caption lft start" data-x="103"
										data-y="116" data-speed="300" data-start="1700"
										data-easing="easeOutExpo"
										style="top: -80px; visibility: hidden; font-size: 18px; padding: 7px 25px; margin: 0px; border: 1px; line-height: 20px; white-space: nowrap; opacity: 1; left: 103px; transform: scale(1, 1) rotate(0deg);">
										Unlimited Happiness with Business
									</div>
									<div class="tp-caption randomrotate start" data-x="697"
										data-y="52" data-speed="300" data-start="2600"
										data-easing="easeOutExpo"
										style="opacity: 0; transform: scale(1.0487227874808, 1.0487227874808) rotate(2.91443274356425deg) translate(0px, 0px); left: 915.595372938551px; top: 191.407259507105px; visibility: visible;">
										<img src="img/perfect.png" alt="Image 8"
											style="width: 367px; height: 326px;">
									</div>
									<div class="tp-caption rox_caption lft start" data-x="103"
										data-y="161" data-speed="300" data-start="2000"
										data-easing="easeOutExpo"
										style="top: -80px; visibility: hidden; font-size: 18px; padding: 7px 25px; margin: 0px; border: 1px; line-height: 20px; white-space: nowrap; opacity: 1; left: 103px; transform: scale(1, 1) rotate(0deg);">
										Boostup Your Business with Us
									</div>
									<div class="tp-caption rox_button lft start" data-x="103"
										data-y="317" data-speed="300" data-start="2300"
										data-easing="easeOutExpo"
										style="top: -104px; visibility: hidden; font-size: 16px; padding: 0px; margin: 0px; border: 0px; line-height: 20px; white-space: nowrap; opacity: 1; left: 103px; transform: scale(1, 1) rotate(0deg);">
										<div class="btnWrap">
											<a class="nemobtn"
												href="http://demo2.themerox.com/joomla/nemo/#">Check
												More</a>
										</div>
									</div>
								</li>
							</ul>
							<div class="tp-bannertimer"
								style="width: 48.880480469632%; overflow: hidden;"></div>
							<div class="tp-loader" style="display: none;"></div>
						</div>
					</div>
					<!-- END REVOLUTION SLIDER -->
				</div>
			</div>
		</div>
		</section>
		<!--轮播图结束-->
		<!--中间介绍开始-->
		<section id="sp-custom_title-wrapper"
			class="animation scaleUp animated">
		<div class="container">
			<div class="row-fluid" id="custom_title">
				<div id="sp-title1" class="span12">
					<div class="module ">
						<div class="mod-wrapper clearfix">
							<h3 class="header">
								<span><FONT FACE="myfont2">一片认真思考的云 
								</span>
							</h3>
							<div class="mod-content clearfix">
								<div class="mod-inner clearfix">
									<div class="custom">
										<p class="text-center">
											<FONT FACE="myfont">当别人在思考如何包装云概念的时候，我们回到了云的源头，什么是真正的云：一个更好的服务器？一个在线U盘？一个容器？还是一个新<br>的世界，我们的思考回到了0和1的起点、我们的思考回到了第一台计算机的诞生，那时，世界是一片空白！




											
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="gap"></div>
				</div>
			</div>
		</div>
		</section>
		<!--中间介绍结束-->
		<!--功能介绍开始-->
		<section id="sp-custom_text-wrapper"
			class="animation periodic animated">
		<div class="container">
			<div class="row-fluid" id="custom_text">
				<div id="sp-custom_text" class="span12">
					<div class="module ">
						<div class="mod-wrapper clearfix">
							<div class="mod-content clearfix">
								<div class="mod-inner clearfix">
									<div class="module">
										<div class="rox-user-inner clearfix ">
											<!-- Start User Position -->
											<div class="span4 animation fadeInLeftBig rox_left0 animated">
												<div
													class="content_padding rox_border_left rox_border_top rox_border_bottom clearfix">
													<i class=""><img src="img/windows.png"> </i>
													<h3 class="rox_header">
														<FONT FACE="myfont">windows</FONT>
													</h3>
													<p>
														<FONT FACE="myfont">一个令人惊讶的在线windows操作系统，你可以在上面做一切你想做的事情，比如当作一个web服务器，比如用它来玩游戏、看电影，或者你还可以用它来储存你的一切珍贵资料，这一切运行的是如此流畅！</FONT>
													</p>
													<div class="btnWrap">
														<a class="nemobtn" href="login.htm"> <FONT
															FACE="myfont">启动</FONT> </a>
													</div>
												</div>
											</div>
											<div class="span4 rox_left0">
												<div
													class="content_padding clearfix  rox_border_left rox_border_top rox_border_bottom rox_border_right">
													<i class=""><img src="img/and.png"> </i>
													<h3 class="rox_header">
														<FONT FACE="myfont">Android</FONT>
													</h3>
													<p>
														<FONT FACE="myfont">一个在线版本的安卓手机，你想用这个手机打电话，别傻了，你可以用他来干一些其他的更酷的事情！比如，用它来测试你开发的安卓软件，用他来玩手机游戏，等等，你还可以做的更多！</FONT>
													</p>
													<div class="btnWrap">
														<a class="nemobtn" href="login.htm"> <FONT
															FACE="myfont">启动</FONT> </a>
													</div>
												</div>
											</div>
											<div class="span4 rox_left0">
												<div
													class="content_padding rox_border_top rox_border_bottom rox_border_right clearfix">
													<i class=""><img src="img/apple.png"> </i>
													<h3 class="rox_header">
														<FONT FACE="myfont">linux</FONT>
													</h3>
													<p>
														<FONT FACE="myfont">在线的linux操作系统，除了当作服务器外，老师们可以用来授课，学生可以在上面尽情练习，而这一切都是免费的，如果你是第一次用linux系统，当然欢迎你来尽情体验这个世界上最受欢迎的系统！</FONT>
													</p>
													<div class="btnWrap">
														<a class="nemobtn" href="login.htm"> <FONT
															FACE="myfont">启动</FONT> </a>
													</div>
												</div>
											</div>
											<!-- End User Postion -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="gap"></div>
				</div>
			</div>
		</div>
		</section>
		<section id="sp-main-body-wrapper" class=" ">
		<div class="container">
			<div class="row-fluid" id="main-body">
				<div id="sp-message-area" class="span12">
					<div id="system-message-container">
						<div id="system-message">
						</div>
					</div>
				</div>
			</div>
		</div>
		</section>
		<!--功能介绍结束-->
		<!--底部开始-->
		<footer id="sp-footer-wrapper" class=" ">
		<div class="container">
			<div class="row-fluid" id="footer">
				<div id="sp-footer1" class="span7">
					<a class="copyright" href="#"
						alt="Free web templates | Bootstrap templates | Premuim Quality templates">
					</a><span class="copyright">Copyright © 2015 nwyun.com. All
						Rights Reserved.</span>
				</div>
				<div id="sp-footer2" class="span5">
					<a class="sp-totop" href="javascript:;" title="Goto Top"
						rel="nofollow" style="display: none;"><small>Goto Top
					</small><i class="icon-caret-up"></i> </a>
				</div>
			</div>
		</div>
		</footer>
		<!--底部结束-->
</body>
<!--主体结束-->
</html>
