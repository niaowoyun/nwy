package com.nwy.orm;

import java.util.Date;

public class UserApplication {
	private String nid;
	private String userId;
	private int systemOs;
	private int systemUserType;
	private String applDesc;
	private Date applTime;

	public String getNid() {
		return nid;
	}

	public void setNid(String nid) {
		this.nid = nid;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getSystemOs() {
		return systemOs;
	}

	public void setSystemOs(int systemOs) {
		this.systemOs = systemOs;
	}

	public int getSystemUserType() {
		return systemUserType;
	}

	public void setSystemUserType(int systemUserType) {
		this.systemUserType = systemUserType;
	}

	public String getApplDesc() {
		return applDesc;
	}

	public void setApplDesc(String applDesc) {
		this.applDesc = applDesc;
	}

	public Date getApplTime() {
		return applTime;
	}

	public void setApplTime(Date applTime) {
		this.applTime = applTime;
	}

}
