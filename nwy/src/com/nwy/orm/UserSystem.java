package com.nwy.orm;

import java.util.Date;

public class UserSystem {
	private String nid;
	private String userId;
	private String systemName;
	private int systemUserType;
	private int systemType;
	private int systemOs;
	private Date systemLoginLateTime;
	private Date systemLogoutLateTime;
	private Boolean isDel;
	private Date createTime;

	public String getNid() {
		return nid;
	}

	public void setNid(String nid) {
		this.nid = nid;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public int getSystemUserType() {
		return systemUserType;
	}

	public void setSystemUserType(int systemUserType) {
		this.systemUserType = systemUserType;
	}

	public int getSystemType() {
		return systemType;
	}

	public void setSystemType(int systemType) {
		this.systemType = systemType;
	}

	public int getSystemOs() {
		return systemOs;
	}

	public void setSystemOs(int systemOs) {
		this.systemOs = systemOs;
	}

	public Date getSystemLoginLateTime() {
		return systemLoginLateTime;
	}

	public void setSystemLoginLateTime(Date systemLoginLateTime) {
		this.systemLoginLateTime = systemLoginLateTime;
	}

	public Date getSystemLogoutLateTime() {
		return systemLogoutLateTime;
	}

	public void setSystemLogoutLateTime(Date systemLogoutLateTime) {
		this.systemLogoutLateTime = systemLogoutLateTime;
	}

	public Boolean getIsDel() {
		return isDel;
	}

	public void setIsDel(Boolean isDel) {
		this.isDel = isDel;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
